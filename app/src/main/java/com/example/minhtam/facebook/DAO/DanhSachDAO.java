package com.example.minhtam.facebook.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.minhtam.facebook.DTO.DanhSachDTO;
import com.example.minhtam.facebook.Database.CreateDatabase;

public class DanhSachDAO {
    SQLiteDatabase database;

    public DanhSachDAO(Context context) {
        CreateDatabase createDatabase = new CreateDatabase(context);
        database = createDatabase.open();
    }

    public long ThemDanhSach(DanhSachDTO danhSachDTO) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CreateDatabase.TB_DANHSACH_TENDANGNHAP, danhSachDTO.getTENDANGNHAP());
        contentValues.put(CreateDatabase.TB_DANHSACH_MATKHAU, danhSachDTO.getTENDANGNHAP());

        long kiemtra = database.insert(CreateDatabase.TB_DANHSACH, null, contentValues);
        return kiemtra;
    }
}
