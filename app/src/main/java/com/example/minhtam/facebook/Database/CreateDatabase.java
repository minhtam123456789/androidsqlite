package com.example.minhtam.facebook.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CreateDatabase extends SQLiteOpenHelper{

    // tạo bảng
    public static String TB_DANHSACH = "DANHSACH";

    // tạo trường trong bảng
    public static String TB_DANHSACH_ID = "ID";
    public static String TB_DANHSACH_TENDANGNHAP = "TENDANGNHAP";
    public static String TB_DANHSACH_MATKHAU = "MATKHAU";


    public CreateDatabase(Context context) {
        super(context, "Facebook", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String tbDANHSACH = "CREATE TABLE " + TB_DANHSACH + " ( " + TB_DANHSACH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + TB_DANHSACH_TENDANGNHAP + " TEXT, "
                + TB_DANHSACH_MATKHAU + " TEXT) ";


        // thực thi
        sqLiteDatabase.execSQL(tbDANHSACH);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    // mở kết nối CSDL
    public SQLiteDatabase open(){
        return this.getWritableDatabase();
    }

}
